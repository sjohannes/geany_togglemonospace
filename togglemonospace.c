/*
 * Copyright (c) 2013, 2016 Johannes Sasongko <sasongko@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


#include <geanyplugin.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) x __attribute__((unused))
#elif defined(_MSC_VER)
# define UNUSED(x)
#else
# define UNUSED(x) x
#endif


GeanyPlugin *geany_plugin;
GeanyData *geany_data;

static bool config_dirty = false;
static char *config_path = NULL;
static GtkWidget *menu_item = NULL;
static char *monospace_font = NULL;
static char *proportional_font = NULL;


// Utilities

// TODO: Schedule this.
static void save_config() {
	if (!config_dirty) return;

	GKeyFile *config = g_key_file_new();
	g_key_file_set_string(config, "togglemonospace", "monospace_font", monospace_font);
	g_key_file_set_string(config, "togglemonospace", "proportional_font", proportional_font);

	gchar *config_dir = g_path_get_dirname(config_path);
	if (!g_file_test(config_dir, G_FILE_TEST_IS_DIR) && utils_mkdir(config_dir, true) != 0) {
		dialogs_show_msgbox(GTK_MESSAGE_ERROR, _("Plugin configuration directory could not be created."));
	} else {
		char *config_data = g_key_file_to_data(config, NULL, NULL);
		utils_write_file(config_path, config_data);
		g_free(config_data);
	}
	g_free(config_dir);

	g_key_file_free(config);
	config_dirty = false;
}


// Callbacks

static void menu_item_activate(GtkMenuItem *menu_item, void *UNUSED(user_data)) {
	char *current_font = geany->interface_prefs->editor_font;
	PangoFontDescription *desc = pango_font_description_from_string(current_font);
	const char *family_name = pango_font_description_get_family(desc);
	PangoContext *context = gtk_widget_get_pango_context(GTK_WIDGET(menu_item));
	PangoFontFamily **families;
	int n_families;
	pango_context_list_families(context, &families, &n_families);
	PangoFontFamily *family = NULL;
	for (int i = 0; i < n_families; i++) {
		if (strcmp(pango_font_family_get_name(families[i]), family_name) == 0) {
			family = families[i];
			break;
		}
	}
	bool monospace = family && pango_font_family_is_monospace(family);
	g_free(families);
	pango_font_description_free(desc);

	if (monospace && strcmp(monospace_font, current_font) != 0) {
		free(monospace_font);
		monospace_font = g_strdup(current_font);
		config_dirty = true;
	} else if (!monospace && strcmp(proportional_font, current_font) != 0) {
		free(proportional_font);
		proportional_font = g_strdup(current_font);
		config_dirty = true;
	}
	ui_set_editor_font(monospace ? proportional_font : monospace_font);
}

static void keybindings_callback(unsigned int UNUSED(key_id)) {
	gtk_menu_item_activate(GTK_MENU_ITEM(menu_item));
}


// Plugin API

PLUGIN_VERSION_CHECK(GEANY_API_VERSION)

PLUGIN_SET_INFO(
	"Toggle monospace",
	"Switch between monospace and proportional fonts",
	"0.2",
	"Johannes Sasongko <sasongko@gmail.com>")

void plugin_init(GeanyData *UNUSED(data)) {
	GKeyFile *config = g_key_file_new();
	config_path = g_strconcat(geany->app->configdir, G_DIR_SEPARATOR_S "plugins" G_DIR_SEPARATOR_S "togglemonospace.conf", NULL);
	g_key_file_load_from_file(config, config_path, G_KEY_FILE_NONE, NULL);
	monospace_font = utils_get_setting_string(config, "togglemonospace", "monospace_font", "Monospace 10");
	proportional_font = utils_get_setting_string(config, "togglemonospace", "proportional_font", "Sans 10");
	g_key_file_free(config);

	menu_item = gtk_menu_item_new_with_mnemonic(_("Toggle _monospace font"));
	ui_add_document_sensitive(menu_item);
	g_signal_connect(menu_item, "activate", G_CALLBACK(menu_item_activate), NULL);
	gtk_container_add(GTK_CONTAINER(geany->main_widgets->tools_menu), menu_item);
	gtk_widget_show(menu_item);

	GeanyKeyGroup *keygroup = plugin_set_key_group(geany_plugin, "togglemonospace", 1, NULL);
	keybindings_set_item(keygroup, 0, keybindings_callback, 0, 0, "toggle", "Toggle monospace", menu_item);
}

void plugin_cleanup(void) {
	save_config();

	free(config_path);
	gtk_widget_destroy(menu_item);
	free(monospace_font);
	free(proportional_font);
}


// vim: noet sts=4 sw=4 ts=4
