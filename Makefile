NAME = togglemonospace

INSTALL ?= install
PKG_CONFIG ?= pkg-config
RM ?= rm -f

CPPFLAGS_GEANY != $(PKG_CONFIG) geany --cflags
LDLIBS_GEANY != $(PKG_CONFIG) geany --libs
PREFIX_GEANY != $(PKG_CONFIG) geany --variable=prefix

CPPFLAGS_OURS = -Wall -Wextra -Wformat=2 -Wpedantic $(CPPFLAGS_GEANY)
CFLAGS_OURS = -O2 -pipe -std=c11
LDFLAGS_OURS = -Wl,-O1,--as-needed -Wl,--no-undefined
LDLIBS_OURS = $(LDLIBS_GEANY)

PREFIX ?= $(PREFIX_GEANY)
INSTALLDIR = $(DESTDIR)$(PREFIX)/share/geany/plugins

all: $(NAME).so

$(NAME).so: $(NAME).c
	$(CC) --shared -fPIC $(CPPFLAGS_OURS) $(CPPFLAGS) $(CFLAGS_OURS) $(CFLAGS) $(LDFLAGS_OURS) $(LDFLAGS) $< $(LDLIBS_OURS) $(LDLIBS) -o $@

clean:
	$(RM) $(NAME).so

install:
	$(INSTALL) -D $(NAME).so $(INSTALLDIR)/$(NAME).so

uninstall:
	$(RM) $(INSTALLDIR)/$(NAME).so

.PHONY: all clean install uninstall
