# geany_togglemonospace

Geany plugin to switch between monospace and proportional fonts.

This plugin requires a modified Geany which exposes `ui_set_editor_font`.
